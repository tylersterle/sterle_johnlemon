﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public GameObject antiJohnLemon;
    public GameObject westHall;
    public GameObject hole;
    public GameObject pressurePlate;
    public float turnSpeed = 20f;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Animator m_Animator;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;

    public GameObject noteHolder;
    public Text noteText;
    public bool noteOpened = false;



    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("isWalking", isWalking);
        
        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    private void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.X) && noteOpened)
        {
            CloseNotePanel();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Note"))
        {
            Notes nScript = other.gameObject.GetComponent<Notes>();
            OpenNotePanel(nScript.noteContents);
        }

        if (other.gameObject == antiJohnLemon)
        {
            antiJohnLemon.GetComponent<PlayerMovement>().enabled = true;
        }

        if (other.gameObject.CompareTag("West_Hall_Activator"))
        {
            westHall.SetActive(true);
        }

        if (other.gameObject.CompareTag("PressurePlate"))
        {
            hole.SetActive(true);
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("PressurePlate"))
        {
            hole.SetActive(false);
        }
    }

    void OpenNotePanel(string text)
    {
        noteOpened = true;

        Time.timeScale = 0f;

        noteText.text = text;

        noteHolder.SetActive(true);
    }

    void CloseNotePanel()
    {
        noteOpened = false;

        Time.timeScale = 1f;

        noteHolder.SetActive(false);
    }
}
