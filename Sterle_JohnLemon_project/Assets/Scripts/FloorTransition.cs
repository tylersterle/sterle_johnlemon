﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorTransition : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public CanvasGroup transitionImageCanvasGroup;
    public Vector3 transition = new Vector3();
    public GameObject player;
    float m_Timer;
    bool transitioning;



    private void Update()
    {
        if (transitioning)
        {
            ScreenFadeIn(transitionImageCanvasGroup);
            player.transform.position = transition;
            if (transitionImageCanvasGroup.alpha == 1)
            {
                if (transitionImageCanvasGroup.alpha == 0)
                {
                    transitioning = false;
                }
                ScreenFadeOut(transitionImageCanvasGroup);
            } 
            transitioning = false;
        }
            
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            transitioning = true;
        }
    }

    void ScreenFadeIn(CanvasGroup imageCanvasGroup)
    {

        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;
        
    }

    void ScreenFadeOut(CanvasGroup imageCanvasGroup)
    {

        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer * fadeDuration;

    }
}
