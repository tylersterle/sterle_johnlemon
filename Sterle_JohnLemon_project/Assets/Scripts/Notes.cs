﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Notes : MonoBehaviour
{
    // Holds the value of the text to be placed on a note.
    public string noteContents;
}